$( document ).ready(function() {
	// var $modal = $('#simpleModal'),
	// 	$modalBtn = $('#modalBtn'),
	// 	$closeBtn = $('.closeBtn');
    //
	// $modalBtn.on('click', openModal);
    //
	// $closeBtn.on('click', closeModal);
    //
	// $(window).on('click', clickOutside);
    //
	// function openModal() {
	// 	$modal.show();
	// }
	// function closeModal() {
	// 	$modal.hide();
	// }
    //
	// function clickOutside(e) {
	// 	if (e.target.id == $modal.attr('id')) {
	// 		$modal.hide();
	// 	}
	// }



    var n = 4; // Number of disks
    var ConfigurationCounter = 0;
    var num = disk(n, 'A', 'C', 'B');  // A, B and C are names of rods

    function disk(n,  from_rod,  to_rod,  aux_rod)
    {
        ConfigurationCounter++;
        if (n == 1)
        {
            console.log("\n Move disk 1 from rod " + from_rod + " to " + to_rod);
            return;
        }
        disk(n-1, from_rod, aux_rod, to_rod);
        console.log("\n Move disk " + n + " from rod " + from_rod + " to rod " + to_rod);
        disk(n-1, aux_rod, to_rod, from_rod);

    }

});
