(function() {
	$body = $('.container');
	$quotes = $('.quotes');
	$author = $('.author');
	$heading = $('.heading');
	$time = $('.time');
	showTime();
	getChromeData();

	function getChromeData() {
		chrome.storage.sync.get('tabs', function(data) {

			data = data['tabs'];

			$heading.text(data['greeting'] +'! '+ data['name']);

			if(data['quote']) {
				$.ajax({
					url: 'https://talaikis.com/api/quotes/random/',
					success: function(result) {
						$quotes.text(result.quote);
						$author.text(result.author);
					}
				});
			} else {
				$('#quote').hide();
			}

		});
	}



	//rendering random background image
	var imgName = "../images/image" + Math.floor((Math.random() * 8) + 1) + '.jpg';
	$body.css("background-image", "url(" + imgName + ")");

	setInterval(function() {
		showTime();
	}, 1000);


	function showTime() {
		var now = new Date();
		var h = now.getHours();
		var m = now.getMinutes();
		var s = now.getSeconds();
		$time.text(h +":"+ m +":"+ s);
	}

})();
