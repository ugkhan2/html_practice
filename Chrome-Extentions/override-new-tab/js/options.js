(function() {
	$body = $('.container');
	$form = $('.user-action');
	$greeting = $('#greeting-message');
	$name = $('#name');
	$quote = $('.quote-option');
	getChromeData();

	function getChromeData() {
		chrome.storage.sync.get('tabs', function(data) {
			data = data['tabs'];

			$greeting.val(data['greeting']);
			$name.val(data['name']);
			$quote.prop('checked', data['quote']);

		});
	}
	//implement configuration settings.

	$form.on("submit", function(e) {
		e.preventDefault();
		var data = $(this).serializeArray();
		data = objectifyForm(data);
		if(data['greeting'] === '') {
			data['greeting'] = "Hello! ";
		}
		if(data['name'] === '') {
			data['name'] = "Stranger";
		}

		if(data['quote'] === '') {
			data['quote'] = "off";
		}
		chrome.storage.sync.set({'tabs': data});

		var notifOptions = {
			type: 'basic',
			iconUrl: '../images/tab.png',
			title: 'Tab Configuration',
			message: 'Your settings are saved.',
		};

		chrome.notifications.create('config', notifOptions);

	});



	function objectifyForm(formArray) {//serialize data function
		var returnArray = {};
		for (var i = 0; i < formArray.length; i++){
			returnArray[formArray[i]['name']] = formArray[i]['value'];
		}
		return returnArray;
	}
})();
